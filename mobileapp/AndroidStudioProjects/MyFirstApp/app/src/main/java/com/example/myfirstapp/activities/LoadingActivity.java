package com.example.myfirstapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myfirstapp.API.RetrofitClient;
import com.example.myfirstapp.Model.PredictResponse;
import com.example.myfirstapp.R;
import com.example.myfirstapp.Storage.SharedPrefManager;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadingActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "filepath";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        /*
            The Loading activity is displayed to the user whilst
            they wait for their prediction to be returned.

            The method makes an api call to get the classification prediction
            of the image previously submitted by the user.

            If the prediction is less than or equal to 50, the user is sent to
            the "good" result activity.

            If the prediction is greater than 50, the user is sent to
            the "bad" result activity.

            The format of the prediction is trimmed in to a readable format
            and sent to the respective activity.

         */

        // This code is used to set the status bar background to transparent
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        String token = SharedPrefManager.getInstance(getApplicationContext()).getToken();
        String bearer = ("Bearer " + token);
        Call<PredictResponse> call = RetrofitClient
                .getInstanceToken(bearer)
                .getApi()
                .getPrediction();

        call.enqueue(new Callback<PredictResponse>() {
            @Override
            public void onResponse(Call<PredictResponse> call, Response<PredictResponse> response) {
                if (response.code() == 200) {
                    PredictResponse predictResponse = response.body();
                    String image = predictResponse.getImage();
                    Float pred = Float.parseFloat(predictResponse.getPred());

                    if (pred <= 50.0f) {
                        Intent goodIntent = new Intent(getApplicationContext(), goodresultActivity.class);
                        String strPred = String.valueOf(pred);
                        goodIntent.putExtra(EXTRA_MESSAGE, new String[] {strPred.replaceAll("\\.0+$", ""), image});
                        startActivity(goodIntent);
                    }
                    else{
                        Intent badIntent = new Intent(getApplicationContext(), resultbadActivity.class);
                        String strPred = String.valueOf(pred);
                        badIntent.putExtra(EXTRA_MESSAGE, new String[] {strPred.replaceAll("\\.0+$", ""), image});
                        startActivity(badIntent);
                    }
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), homeActivity.class);
                    Toast.makeText(LoadingActivity.this, "Failed to get prediction, please try send your image again", Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                }
            }
            @Override
            public void onFailure(Call<PredictResponse> call, Throwable t) {
                Log.d("Get Prediction", t.getMessage());
            }
        });
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    protected void onStart(){
        super.onStart();
        /*
         * The onStart() method checks if the user is logged in upon starting the activity.
         * If they aren't the user is sent back to the MainActivity(Login activity)
         * */
        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        /*
            The onBackPressed() method is called when operating system "back"(<)
            button is pressed. This button sends the user back to the home page.
         */
        startActivity(new Intent(this, homeActivity.class));
    }

}
