package com.example.myfirstapp;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfirstapp.Model.RecordResponse;


import java.util.List;

public class CustomListAdapter extends ArrayAdapter {
    /*
        This class is used to generate a list of all user records.
        It takes the response from recordResponse and creates a row view for each
        record related to the user.

        If a record has null values for the date or prediction fields, they are
        removed from the response list(not the database) meaning only classified image
        records are listed.
     */

    private final Activity context;
    private final List<RecordResponse> records;

    public CustomListAdapter(Activity context, List<RecordResponse> records){
        super(context,R.layout.listview_row , records);

        this.context = context;
        this.records = records;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.listview_row, null,true);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);
        TextView nameTextField = (TextView) rowView.findViewById(R.id.nameView);
        TextView infoTextField = (TextView) rowView.findViewById(R.id.infoView);

        if(records.get(position).getPred() == null || records.get(position).getDate() == null) {
            records.remove(records.get(position));
            notifyDataSetChanged();
        }
        else{
            Float pred = Float.parseFloat(records.get(position).getPred());
            if (pred <= 50.0f) {
                infoTextField.setText("Diagnosis: " + context.getResources().getString(R.string.benign));
            }
            else {
                infoTextField.setText("Diagnosis: " + context.getResources().getString(R.string.malignant));
            }
            nameTextField.setText("Created: " + records.get(position).getDate());
            Bitmap image = BitmapFactory.decodeResource(context.getResources(), R.drawable.syringe);
            imageView.setImageBitmap(image);
        }
        return rowView;
    }
}

