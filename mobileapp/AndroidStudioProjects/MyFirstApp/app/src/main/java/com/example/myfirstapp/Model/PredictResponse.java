package com.example.myfirstapp.Model;

import com.google.gson.annotations.SerializedName;


public class PredictResponse {

    @SerializedName("prediction")
    private String pred;

    @SerializedName("image")
    private String image;

    public PredictResponse(String pred, String image) {
        this.pred = pred;
        this.image = image;
    }

    public String getImage(){return image;}

    public String getPred() {
        return pred;
    }
}
