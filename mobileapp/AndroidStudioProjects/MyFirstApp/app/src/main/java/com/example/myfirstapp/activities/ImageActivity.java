package com.example.myfirstapp.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.myfirstapp.API.RetrofitClient;
import com.example.myfirstapp.Model.ImageResponse;
import com.example.myfirstapp.R;
import com.example.myfirstapp.Storage.SharedPrefManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.myfirstapp.activities.homeActivity.EXTRA_MESSAGE;

public class ImageActivity extends AppCompatActivity {

    /*
    * The Image activity displays the image the user would like to use to them.
    * An example image is also shown to the user so they know what an ideal image
    * looks like.
    * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        // This code is used to set the status bar background to transparent
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        Intent intent = getIntent();
        String filepath = intent.getStringExtra(EXTRA_MESSAGE);

        File imgFile = new  File(filepath);
        Bitmap img = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        Bitmap image = null;
        try {
            image = modifyOrientation(img, filepath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ImageView myImage = (ImageView) findViewById(R.id.imageView);
        myImage.setImageBitmap(image);
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void sendImage(View view){
        /*
         * This method is used to retrieve the image the user selected then resize,
         * encode the image to base64 and then make a call to the api to create a new
         * diagnosis for the user.
         *
         * If the api call is successful, the user is notified via a toast(on screen bubble notification)
         * If the api call is unsuccessful, the user is notified via a toast
         * */
        ImageView lesionImage = new ImageView(this);
        lesionImage.setImageResource(R.drawable.lesion_image);

        AlertDialog.Builder termsDialogBuilder = new AlertDialog.Builder(this);
        termsDialogBuilder.setTitle(R.string.note);
        termsDialogBuilder.setMessage(getString(R.string.image_check));
        termsDialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {

            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent oldIntent = getIntent();
                String filepath = oldIntent.getStringExtra(EXTRA_MESSAGE);

                Bitmap myBitmap = BitmapFactory.decodeFile(filepath);
                Bitmap resized = Bitmap.createScaledBitmap(myBitmap, 600, 450, true);

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                resized.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();

                String encoded = Base64.getEncoder().encodeToString(byteArray);

                String token = SharedPrefManager.getInstance(getApplicationContext()).getToken();
                String bearer = ("Bearer " + token);

                Call<ImageResponse> call = RetrofitClient
                        .getInstanceToken(bearer)
                        .getApi()
                        .sendImage(encoded);

                call.enqueue(new Callback<ImageResponse>() {
                    @Override
                    public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                        if (response.code() == 201) {
                            Intent intent = new Intent(getApplicationContext(), LoadingActivity.class);
                            Toast.makeText(ImageActivity.this, "Image sent, please wait for your prediction", Toast.LENGTH_SHORT).show();
                            startActivity(intent);
                        }
                        else {
                            Toast.makeText(ImageActivity.this, "Image failed to send, please try again", Toast.LENGTH_SHORT).show();
                            System.out.println(response.toString());
                        }
                    }
                    @Override
                    public void onFailure(Call<ImageResponse> call, Throwable t) {
                        Log.d("Image Send", t.getMessage());
                    }
                });
            }
           });
        termsDialogBuilder.setView(lesionImage);

        termsDialogBuilder.setNegativeButton(R.string.no , new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        AlertDialog alertDialog = termsDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onStart(){
        super.onStart();
        /*
         * The onStart() method checks if the user is logged in upon starting the activity.
         * If they aren't the user is sent back to the MainActivity(Login activity)
         * */

        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }


    public void retakeImage(View view){
        /*
            The retakeImage() method allows the user to go back to the
            home screen if they would like to retake their image.
         */
        Intent intent = new Intent( this, homeActivity.class);
        startActivity(intent);
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        /*
            The modifyOrientation() method is used to find the rotation of the image
            and display the image at the orientation that the user is using the device
         */
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        /*
            The rotate() method is called in the modifyOrientation() method
            to rotate the image to match the interface.
         */
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    @Override
    public void onBackPressed() {
        /*
            The onBackPressed() method is called when operating system "back"(<)
            button is pressed. This button sends the user back to the home page.
         */
        startActivity(new Intent(this, homeActivity.class));
    }

}


