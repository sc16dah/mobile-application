package com.example.myfirstapp.Model;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;


public class RecordResponse implements Serializable {


    @SerializedName("t_diagnosis")
    private String t_diagnosis;

    @SerializedName("date_submitted")
    private String date_submitted;


    public RecordResponse(String t_diagnosis, JsonObject id, String date_registered) {
        this.t_diagnosis = t_diagnosis;
        this.date_submitted = date_registered;
    }

    public String getPred() { return t_diagnosis;}

    public String getDate() { return formatDate(date_submitted); }

    private String formatDate(String date) {
        if (date != null) {
            String[] components = date.split(",");

            if (components.length >= 3) {
                return components[2] + "." + components[1] + "." + components[0];
            }
        }
        return date;
    }

}



