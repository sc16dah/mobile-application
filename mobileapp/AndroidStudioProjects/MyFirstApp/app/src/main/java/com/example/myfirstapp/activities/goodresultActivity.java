package com.example.myfirstapp.activities;

import androidx.appcompat.app.AppCompatActivity;
import com.example.myfirstapp.R;
import com.example.myfirstapp.Storage.SharedPrefManager;

import android.app.Activity;
import android.content.Intent;
import android.util.Base64;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import android.os.Build;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.ImageView;
import android.widget.TextView;


import static com.example.myfirstapp.activities.LoadingActivity.EXTRA_MESSAGE;

public class goodresultActivity extends AppCompatActivity {
    /*
        The goodResultActivity is displayed to the user when their prediction
        is less than or equal to 50.

        The activity displays the users image and prediction.
        The activity also informs the user on some second steps regarding
        the predction.

        The user can also navigate back to the homeActivity with the button
        underneath the second step information
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goodresult);

        // This code is used to set the status bar background to transparent
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        Intent intent = getIntent();
        String[] predictionString = intent.getStringArrayExtra(EXTRA_MESSAGE);
        String prediction = predictionString[0];
        String image = predictionString[1];

        byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

        ImageView myImage = (ImageView) findViewById(R.id.userImageView);
        myImage.setImageBitmap(decodedByte);

        TextView predictionView = findViewById(R.id.prediction);
        predictionView.setText(prediction + "%");

    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    public void homePage(View view){
        /*
            Method assigned to the homeActivity button.
         */
        startActivity(new Intent(this, homeActivity.class));
    }

    @Override
    public void onBackPressed() {
        /*
            The onBackPressed() method is called when operating system "back"(<)
            button is pressed. This button sends the user back to the home page.
         */
        startActivity(new Intent(this, homeActivity.class));
    }

    @Override
    protected void onStart(){
        super.onStart();
        /*
         * The onStart() method checks if the user is logged in upon starting the activity.
         * If they aren't the user is sent back to the MainActivity(Login activity)
         * */
        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

}
