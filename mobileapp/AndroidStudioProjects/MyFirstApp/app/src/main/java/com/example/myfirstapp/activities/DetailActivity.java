package com.example.myfirstapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myfirstapp.Model.RecordResponse;
import com.example.myfirstapp.R;
import com.example.myfirstapp.Storage.SharedPrefManager;

import java.io.Serializable;

public class DetailActivity extends AppCompatActivity implements Serializable {

    /*
    * The detail activity displays information about the particular diagnosis record selected
    * on the records activity.
    *
    * The information displayed is the date the diagnosis was submitted, and the classification
    * along with the prediction of the image.
    *
    * The function at the bottom clearData is the UI code for deleting a record
    * */


    private RecordResponse diagnosis;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        // This code is used to set the status bar background to transparent
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        diagnosis = (RecordResponse) getIntent().getSerializableExtra("com.example.myfirstapp.Model.RecordResponse");

        TextView dateSubmittedText = (TextView) findViewById(R.id.dateView);
        TextView diagnosisText = (TextView) findViewById(R.id.diagnosisView);
        ImageView imageView = (ImageView) findViewById(R.id.diagnosisImage);

        dateSubmittedText.setText(diagnosis.getDate());
        Float pred = Float.parseFloat(diagnosis.getPred());

        if (pred <= 50.0f) {
            diagnosisText.setText(diagnosis.getPred() + "% - " + context.getResources().getString(R.string.benign));
        }
        else {
            diagnosisText.setText(diagnosis.getPred() + "% - " + context.getResources().getString(R.string.malignant));
        }
    }

    public void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    protected void onStart(){
        super.onStart();
        /*
        * The onStart() method checks if the user is logged in upon starting the activity.
        * If they aren't the user is sent back to the MainActivity(Login activity)
        * */

        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

//    public void clearData(View view) {
//        AlertDialog.Builder clearDialogBuilder = new AlertDialog.Builder(this);
//        clearDialogBuilder.setTitle("Are you sure you want to delete the record?");
//        clearDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface arg0, int arg1) {
//                //Delete record from database
//                Intent intent = new Intent(DetailActivity.this, recordsActivity.class);
//                startActivity(intent);
//                Toast.makeText(DetailActivity.this, "The record has been successfully deleted", Toast.LENGTH_LONG).show();
//            }
//        });
//        clearDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
//                startActivity(intent);
//
//            }
//        });
//
//        AlertDialog alertDialog = clearDialogBuilder.create();
//        alertDialog.show();
//
//    }
}

