package com.example.myfirstapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;

import android.os.Build;
import android.os.Bundle;

import com.example.myfirstapp.API.RetrofitClient;
import com.example.myfirstapp.Model.DefaultResponse;
import com.example.myfirstapp.R;
import com.example.myfirstapp.Storage.SharedPrefManager;

import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText editTextEmail, editTextPassword, editTextName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        /*
            The registerActivity is the app that allows users to
            create a new account for use of the app.
         */

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextName = findViewById(R.id.editTextName);

        findViewById(R.id.registerButton).setOnClickListener(this);
        findViewById(R.id.loginButton).setOnClickListener(this);


        // This code is used to set the status bar background to transparent
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        ArrayList<String> years = new ArrayList<String>();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        currentYear = (currentYear - 18);
        for(int i = currentYear; i >= 1900; i-=1){
            years.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapterYear = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, years);
        Spinner spinYear = (Spinner)findViewById(R.id.yearspin);
        spinYear.setAdapter(adapterYear);

        ArrayList<String> days = new ArrayList<String>();
        for(int i = 1; i <= 31; i++){
            days.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapterDay = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, days);
        Spinner spinDay = (Spinner)findViewById(R.id.dayspin);
        spinDay.setAdapter(adapterDay);

        ArrayList<String> months = new ArrayList<String>();
        for(int i = 1; i <= 12; i++){
            months.add(Integer.toString(i));
        }
        ArrayAdapter<String> adapterMonth = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, months);
        Spinner spinmonth = (Spinner) findViewById(R.id.monthspin);
        spinmonth.setAdapter(adapterMonth);
    }

    @Override
    protected void onStart(){
        super.onStart();
        /*
         * The onStart() method checks if the user is logged in upon starting the activity.
         * If they aren't the user is sent back to the MainActivity(Login activity)
         * */
        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    public void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }


    private void userSignUp(String dateOfBirth) {
        /*
            userSignUp() checks the user's input credentials
            to make sure they dont currently exist.

            The method also makes the api call to register the account
            with the credentials entered by the user.
         */
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String name = editTextName.getText().toString().trim();

        System.out.println(dateOfBirth);
        Log.d("date of birth", "Hello hello hello");
        Log.d("date of birth", dateOfBirth);

        System.out.println("Made it to the user sign up method");

        if (email.isEmpty()) {
            editTextEmail.setError("Email is required");
            editTextEmail.requestFocus();
            return;
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError("Enter a valid email");
            editTextEmail.requestFocus();
            return;
        }
        else if (password.isEmpty()) {
            editTextPassword.setError("Password is required");
            editTextPassword.requestFocus();
            return;
        }
        else if (name.isEmpty()) {
            editTextName.setError("Name is required");
            editTextName.requestFocus();
            return;
        }
        else if (password.length() < 8) {
            editTextPassword.setError("Password must be at least 8 characters");
            editTextPassword.requestFocus();
            return;
        }
            Call<DefaultResponse> call = RetrofitClient
                    .getInstance()
                    .getApi()
                    .createUser(email, password, name, dateOfBirth);

            call.enqueue(new Callback<DefaultResponse>() {
                @Override
                public void onResponse(Call<DefaultResponse> call, Response<DefaultResponse> response) {
                    if (response.code() == 201) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        DefaultResponse dr = response.body();
                        Toast.makeText(RegisterActivity.this, dr.getEmailMsg(), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(RegisterActivity.this, "Thank you for Registering", Toast.LENGTH_LONG).show();
                        startActivity(intent);
                    }
                    else {
                        DefaultResponse dr2 = response.body();
                        Toast.makeText(RegisterActivity.this, "Registration Failed", Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<DefaultResponse> call, Throwable t) {
                    Log.d("Register", t.getMessage());
                }
            });

        }


    public void register()
    {
        /*
            register() is called when the user presses the register
            button. It gets the user's input and once they have accepted the
            terms and conditions of the app, the account is made.
         */
        Resources res = getResources();
        String[] terms = res.getStringArray(R.array.terms_and_conditions);
        AlertDialog.Builder termsDialogBuilder = new AlertDialog.Builder(this);
        termsDialogBuilder.setTitle("Terms and Conditions");
        termsDialogBuilder.setMessage(terms[0] + terms[1] + terms[2] + terms[3] + terms[4] + terms[5] + terms[6]);
        termsDialogBuilder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                Spinner yearSpinItem = findViewById(R.id.yearspin);
                Spinner monthSpinItem = findViewById(R.id.monthspin);
                Spinner daySpinItem = findViewById(R.id.dayspin);

                Integer day = Integer.parseInt((String) daySpinItem.getSelectedItem());
                String month = monthSpinItem.getSelectedItem().toString();
                Integer year = Integer.parseInt((String) yearSpinItem.getSelectedItem());

                String dateOfBirth = year + "/" + month + "/" + day;
                System.out.println("Date of Birth = " + dateOfBirth);

                userSignUp(dateOfBirth);

            }
        });

        termsDialogBuilder.setNegativeButton("Decline", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                finish();
                startActivity(intent);
                Toast.makeText(RegisterActivity.this, "Please Accept the Terms and Conditions", Toast.LENGTH_LONG).show();
            }
        });

        AlertDialog alertDialog = termsDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.registerButton:
                register();
                break;
            case R.id.loginButton:
                startActivity(new Intent(this, MainActivity.class));
                break;
        }
    }

}
