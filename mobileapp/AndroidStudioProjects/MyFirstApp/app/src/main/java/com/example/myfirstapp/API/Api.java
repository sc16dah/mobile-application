package com.example.myfirstapp.API;

import com.example.myfirstapp.Model.DefaultResponse;
import com.example.myfirstapp.Model.ExampleResponse;
import com.example.myfirstapp.Model.ImageResponse;
import com.example.myfirstapp.Model.LoginResponse;
import com.example.myfirstapp.Model.PredictResponse;
import com.example.myfirstapp.Model.RecordResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {
    /*
        This lists all of the routes used in the application.
        If a route requires data to be sent(e.g. a POST request)
        data fields will need to be defined.
     */

    @FormUrlEncoded
    @POST("users")
    Call<DefaultResponse> createUser(
            @Field("email") String email,
            @Field("password") String password,
            @Field("name") String name,
            @Field("date_of_birth") String dateOfBirth
    );

    @FormUrlEncoded
    @POST("tokens")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("skin_cancer_analysis")
        Call<ImageResponse> sendImage(
                @Field("photo_base64") String image
    );

    @GET("skin_cancer_analysis")
    Call<PredictResponse> getPrediction();

    @GET("records")
    Call<List<RecordResponse>> getRecords();

    @GET("example/labels")
    Call<ExampleResponse> example(
    );

//    @GET("example/labels")
//
//    Call<ExampleResponse> example(@Header("Bearer") String token
//    );


}
