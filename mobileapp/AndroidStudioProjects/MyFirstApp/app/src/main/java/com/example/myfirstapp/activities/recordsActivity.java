package com.example.myfirstapp.activities;

import androidx.appcompat.app.AppCompatActivity;

import com.example.myfirstapp.API.RetrofitClient;
import com.example.myfirstapp.Model.RecordResponse;
import com.example.myfirstapp.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;

import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.myfirstapp.CustomListAdapter;
import com.example.myfirstapp.Storage.SharedPrefManager;

import java.io.Serializable;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class recordsActivity extends AppCompatActivity implements Serializable {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_records);
        /*
            The recordsActivity displays a list of all the diagnosis records
            related to the user logged in.

            It makes an api call to receive a list of all records and then
            calls the list adapted(CustomListAdapter class) to generate the list

         */

        // This code is used to set the status bar background to transparent
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }

        String token = SharedPrefManager.getInstance(this).getToken();
        String bearer = ("Bearer " + token);
        Call<List<RecordResponse>> call = RetrofitClient
                .getInstanceToken(bearer)
                .getApi()
                .getRecords();

        call.enqueue(new Callback<List<RecordResponse>>() {
            @Override
            public void onResponse(Call<List<RecordResponse>> call, Response<List<RecordResponse>> response) {
                if (response.isSuccessful()) {
                    List<RecordResponse> recordResponse = response.body();
                    setListView(recordResponse);
                }
                else {
                    Toast.makeText(recordsActivity.this, "Failed to Fetch Records", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onFailure(Call<List<RecordResponse>> call, Throwable t) {
                Log.d("Image Send", t.getMessage());
            }
        });
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    @Override
    protected void onStart(){
        super.onStart();
        /*
         * The onStart() method checks if the user is logged in upon starting the activity.
         * If they aren't the user is sent back to the MainActivity(Login activity)
         * */
        if(!SharedPrefManager.getInstance(this).isLoggedIn()){
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    private void setListView(final List<RecordResponse> recordResponse) {
        /*
            setListView() makes the call to the api returning the
            list of all user records. The code generating the rowViews can
            be found in the CustomListAdapter file.

         */
        CustomListAdapter listAdapter = new CustomListAdapter(this, recordResponse);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(listAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                Intent intent = new Intent(recordsActivity.this, DetailActivity.class);
                intent.putExtra("com.example.myfirstapp.Model.RecordResponse", recordResponse.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        /*
            The onBackPressed() method is called when operating system "back"(<)
            button is pressed. This button sends the user back to the home page.
         */
        startActivity(new Intent(this, homeActivity.class));
    }


}

