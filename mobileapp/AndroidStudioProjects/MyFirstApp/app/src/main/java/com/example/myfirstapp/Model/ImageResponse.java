package com.example.myfirstapp.Model;

import android.graphics.Bitmap;
import com.google.gson.annotations.SerializedName;

public class ImageResponse {

    @SerializedName("error")
    private String err;

    @SerializedName("message")
    private String msg;

    @SerializedName("image")
    private Bitmap img;

    public ImageResponse(String err, String msg, Bitmap img) {
        this.err = err;
        this.msg = msg;
        this.img = img;

    }

    public String getErr() {
        return err;
    }

    public String getMsg() {
        return msg;
    }

    public Bitmap getImg() {
        return img;
    }
}


