# Directory containing each machine learning script

## Contains
* directoryScriptv2 - Used to format the directory as required prior to training
* imageAnalyserv2 - Used to create, train and test the model and output the results
in the form of graphs
* predictor - Uses the model created in imageAnalyser to predict on the image found in the "image/data/" file

Note - Each script has been developed on windows through the use of Pycharm. However, the file paths should be system agnostic in each script

Note - This is an intensive script to run. There are a large list of packages used found
in requirements.txt.

## directoryScriptv2
This script is called when imageAnalyserv2 is run, so it doesnt require running first.
The script reqires that the dataset(skin-cancer-mnist-ham10000) available from https://www.kaggle.com/kmader/skin-cancer-mnist-ham10000 is in the same directory as the file.
The dataset needs to be in the same format as it is downloaded in for the script to 
find the files.


## imageAnalyserv2
This is an intensive script to run. There are a large list of packages used found
in requirements.txt. When training the model, tensorflow should attempt to use your
graphics card(gpu). This will not work if you do not have "CUDA" https://developer.nvidia.com/cuda-zone

If this isnt the case, the script will default and use your processor(cpu).
This however can/will take a long time for most cpu's.

The script reqires that the dataset(skin-cancer-mnist-ham10000) available from https://www.kaggle.com/kmader/skin-cancer-mnist-ham10000 is in the same directory as the file.
he dataset needs to be in the same format as it is downloaded in for the script to 
find the files.

## predictor
The only prerequisite for this file is the image being classified has to be in the 
"image/data/" directory. This directory also has to be in the same directory as the script.

An example image for prediction can be found in the repo along with the correct directory structre.
