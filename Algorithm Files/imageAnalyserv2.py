import itertools
import numpy as np
import matplotlib.pyplot as plt
import os
import tensorflow

from shutil import copyfile, rmtree
from keras_preprocessing.image import ImageDataGenerator
from tensorflow_core.python.keras import Sequential
from tensorflow_core.python.keras.callbacks import ModelCheckpoint
from tensorflow_core.python.keras.layers import Conv2D, MaxPooling2D, Dense, Flatten, Dropout
from sklearn.metrics import confusion_matrix

from directoryScriptv2 import structreDirectory

IMG_HEIGHT = 224
IMG_WIDTH = 224
epochs = 12
batch_size = 50

"""
    Takes no arguments
    This is the main file that defines, trains and tests the model
    and then outputs the results of the tests.
    
    The directory has to be as described below:
    The dataset available from https://www.kaggle.com/kmader/skin-cancer-mnist-ham10000
    needs to be in the same directory as this file in the same format as it is downloaded in
    
    
    idenskin\train
    idenskin\test
    
    idenskin\train\malignant
    idenskin\test\malignant
    
    idenskin\train\benign
    idenskin\test\benign
    
    Note: The filepaths that were hardcoded for windows,
    this may mean on over OS's the directory may not be found.
    If so replace the '\' with '/' e.g.
    
    idenskin/train
    idenskin/test
    
    idenskin/train/malignant
    idenskin/test/malignant
    
    idenskin/train/benign
    idenskin/test/benign
    
    This script will output a trained model in to the same directory 'model.h5'
    and will test this model to produce graphs and charts from the results from the tests
    
"""

"""
    augmentData() first calls the directory script to make sure the image directories
    follow the correct format.

    :returns train_data_gen - The training data from the training directory 
             test_data_gen - The test data from the test directory 
             valid_data_gen - The validation data from the test directory 
             total_train_data - The total amount of training data
             total_test_data - The total amount of the test data
"""


def augmentData():
    currentDirectory = os.getcwd()
    sd = structreDirectory()
    sd.formatDirectory()
    sd.sortDirectories()

    # """
    #     The filepaths to the various directories are defined
    #     for later use in the script.
    # """

    trainDirectory = os.path.join(currentDirectory, 'idenskin', 'train')
    testDirectory = os.path.join(currentDirectory, 'idenskin', 'test')

    print('-------------------------------------------------------')

    malignantTrainDirectory = os.path.join(currentDirectory, 'idenskin', 'train', 'malignant')
    total_malignant_train_data = len(os.listdir(malignantTrainDirectory))
    print(total_malignant_train_data)
    benignTrainDirectory = os.path.join(currentDirectory, 'idenskin', 'train', 'benign')
    total_benign_train_data = len(os.listdir(benignTrainDirectory))
    print(total_benign_train_data)

    print('-------------------------------------------------------')

    malignantTestDirectory = os.path.join(currentDirectory, 'idenskin', 'test', 'malignant')
    total_malignant_test_data = len(os.listdir(malignantTestDirectory))
    print(total_malignant_test_data)
    benignTestDirectory = os.path.join(currentDirectory, 'idenskin', 'test', 'benign')
    total_benign_test_data = len(os.listdir(benignTestDirectory))
    print(total_benign_test_data)

    print('-------------------------------------------------------')

    total_train_data = total_malignant_train_data + total_benign_train_data
    total_test_data = total_malignant_test_data + total_benign_test_data

    # """
    #     Here new directories are created to temporarily store the
    #     the augmented files whilst they are copied in to the
    #     respective directory
    # """

    augment_directory = 'augment_directory'
    if not os.path.exists('augment_directory'):
        os.mkdir(augment_directory)

    image_directory = os.path.join(augment_directory, 'image_directory')
    if not os.path.exists(os.path.join('augment_directory', 'image_directory')):
        os.mkdir(image_directory)

    image_list = os.listdir(os.path.join(trainDirectory, 'malignant'))
    for filename in image_list:
        source = os.path.join(trainDirectory, 'malignant', filename)
        destination = os.path.join(image_directory, filename)
        copyfile(source, destination)

    augment_save_path = augment_directory
    current_save_path = os.path.join(trainDirectory, 'malignant')

    # """
    #     The augmented images are generated the same amount
    #     of times to equalise the malignant training set
    #     with the benign training set.
    #
    #     The number of images in each class's training set
    #     are displayed.
    # """

    train_image_generator = ImageDataGenerator(
        rotation_range=180,
        width_shift_range=0.1,
        height_shift_range=0.1,
        zoom_range=0.1,
        horizontal_flip=True,
        vertical_flip=True,
        fill_mode='nearest')

    augment_datagen = train_image_generator.flow_from_directory(augment_save_path,
                                                                save_to_dir=current_save_path,
                                                                save_format='jpg',
                                                                save_prefix='augmented',
                                                                target_size=(224, 224),
                                                                batch_size=batch_size)

    aug_images_wanted = total_benign_train_data
    file_num = len(os.listdir(image_directory))
    batch_num = int(np.ceil((aug_images_wanted - file_num) / batch_size))

    for i in range(0, batch_num):
        imgs, labels = next(augment_datagen)

    rmtree('augment_directory')

    # print('-------------------------------------------------------')
    # print(len(os.listdir(malignantTrainDirectory)))
    # print(len(os.listdir(benignTrainDirectory)))
    # print('-------------------------------------------------------')
    # print(len(os.listdir(malignantTestDirectory)))
    # print(len(os.listdir(benignTestDirectory)))
    # print('-------------------------------------------------------')

    # """
    #     The images are pre-processed ready for training and each
    #     set of data(training, test and validation) are created from
    #     the directories created previously.
    # """

    data_generator = ImageDataGenerator(preprocessing_function= \
                                            tensorflow.keras.applications.mobilenet.preprocess_input)

    train_data_gen = data_generator.flow_from_directory(batch_size=batch_size,
                                                        directory=trainDirectory,
                                                        shuffle=True,
                                                        target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                        class_mode='binary'
                                                        )

    valid_data_gen = data_generator.flow_from_directory(batch_size=batch_size,
                                                        directory=testDirectory,
                                                        shuffle=False,
                                                        target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                        class_mode='binary'
                                                        )

    test_data_gen = data_generator.flow_from_directory(batch_size=batch_size,
                                                       directory=testDirectory,
                                                       shuffle=False,
                                                       target_size=(IMG_HEIGHT, IMG_WIDTH),
                                                       class_mode='binary'
                                                       )

    return {'train_data_gen': train_data_gen, 'test_data_gen': test_data_gen,
            'valid_data_gen': valid_data_gen, 'total_train_data': total_train_data,
            'total_test_data': total_test_data}


"""
    createModel() creates, summerises, compiles and trains the model on the
    training set.
    
    :arg  train_data_gen - The training data from the training directory 
          test_data_gen - The test data from the test directory 
          valid_data_gen - The validation data from the test directory 
          total_train_data - The total amount of training data
          total_test_data - The total amount of the test data
    
    :returns history - the trained model and its results after training and 
                        validation testing
             model - the model entity 
"""


def createModel(train_data_gen, test_data_gen, valid_data_gen, total_train_data, total_test_data):
    train_steps = np.ceil(total_train_data / batch_size)
    valid_steps = np.ceil(int(total_test_data) / batch_size)

    # The model is defined

    model = Sequential([
        Conv2D(64, 3, padding='same', activation='relu', input_shape=(IMG_HEIGHT, IMG_WIDTH, 3)),
        MaxPooling2D(),
        Conv2D(32, 3, padding='same', activation='relu'),
        MaxPooling2D(),
        Conv2D(16, 3, padding='same', activation='relu'),
        MaxPooling2D(),
        Flatten(),
        Dense(64, activation='relu'),
        Dropout(0.25),
        Dense(1, activation='sigmoid')
    ])

    model.summary()

    # Summary of the model is displayed at which point the model is compiled

    model.compile(optimizer='adam',
                  loss=tensorflow.keras.losses.binary_crossentropy,
                  metrics=['accuracy'])

    print(valid_data_gen.class_indices)

    class_weights = {
        0: 1.0,
        1: 1.0,
    }

    # Checkpoints are defined (what happens between epochs)

    path = 'model.h5'
    checkpoint = ModelCheckpoint(path, monitor='val_acc', verbose=1,
                                 save_best_only=True, mode=max)

    callbacks = [checkpoint]

    # The model is trained and evaluated

    history = model.fit_generator(
        train_data_gen,
        steps_per_epoch=train_steps,
        epochs=epochs,
        class_weight=class_weights,
        validation_data=valid_data_gen,
        validation_steps=valid_steps,
        verbose=1,
        callbacks=callbacks
    )

    model.load_weights('model.h5')

    val_loss, val_acc = model.evaluate_generator(test_data_gen, steps=total_test_data)

    print('val_loss:', val_loss)
    print('val_acc:', val_acc)

    return {'history': history, 'model': model}


"""
    plotModel() takes the trained model as input and plots the model
    on to a human readable chart
    
    :argument history - the trained model and its results after training and 
                        validation testing
"""


def plotModel(history):
    acc = history.history['acc']
    val_acc = history.history['val_acc']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure(figsize=(8, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.plot(epochs_range, val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')
    plt.show()


"""
    confusionMatrix() takes the model data and results as argument and 
    creates a confusion matrix based on the results to help understand
    the model's performance
    
    :arg test_data_gen - The test data from the test directory
         total_test_data - The total amount of the test data
         model - the model entity 
"""


def confusionMatrix(test_data_gen, model, total_test_data):
    predictions = model.predict_generator(test_data_gen, total_test_data / batch_size, verbose=1)
    labels = test_data_gen.classes
    print('Labels', labels.shape)
    print('Pred', predictions.shape)

    cm = confusion_matrix(np.round(predictions), labels)

    cm_plot_labels = ['benign', 'malignant']

    plot_confusion_matrix(cm, cm_plot_labels, title='Confusion Matrix')


"""
    plot_confusion_matrix() plots the confusion matrix that based on the data that
    it takes as input
    
        :arg cm - The confusion matrix being plot
             classes - The classes used in the model(Benign and Malignant)
             title - The title of the confusion matrix plot
             cmap = The colour and design of the confusion matrix plot
             normalize - A normalized confusion matrix displays the values between 0 and 1
                        acting as a percentage
"""


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print('Normalized confusion matrix')
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment='center',
                 color='white' if cm[i, j] > thresh else 'black')

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()
    plt.show()


"""
    Main function that calls all methods in succession to
    run the script. 
"""


def main():
    dataGen = augmentData()
    history = createModel(dataGen['train_data_gen'], dataGen['test_data_gen'], dataGen['valid_data_gen'],
                          dataGen['total_train_data'], dataGen['total_test_data'], )
    plotModel(history['history'])
    confusionMatrix(dataGen['test_data_gen'], history['model'], dataGen['total_test_data'])


if __name__ == '__main__':
    main()
