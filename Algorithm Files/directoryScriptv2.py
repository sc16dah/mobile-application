import os
import pandas
from shutil import copyfile, rmtree
from tensorflow_core.contrib.learn.python.learn.estimators._sklearn import train_test_split
from pathlib import Path

""" 
    Takes no arguments
    Script that format's the dataset as required for training and testing
    The script will format the directory as it needs to be format for 
    training the model.
    This class is called when the imageAnalyserv2 training algorithm is run.
    The dataset available from https://www.kaggle.com/kmader/skin-cancer-mnist-ham10000
    needs to be in the same directory as this file in the same format as it is downloaded in
    
    
    idenskin\train
    idenskin\test
    
    idenskin\train\malignant
    idenskin\test\malignant
    
    idenskin\train\benign
    idenskin\test\benign
    
    Note: The filepaths that were hardcoded for windows,
    this may mean on over OS's the directory may not be found.
    If so replace the '\' with '/' e.g.
    
    idenskin/train
    idenskin/test
    
    idenskin/train/malignant
    idenskin/test/malignant
    
    idenskin/train/benign
    idenskin/test/benign
    
"""


class structreDirectory():
    currentDirectory = os.getcwd()

    def formatDirectory(self):
        idenDirectory = os.path.join(self.currentDirectory, Path("idenskin"))
        try:
            if os.path.exists(idenDirectory):
                rmtree(Path('idenskin'))
                print("Removed Directory")
                os.mkdir(idenDirectory)
                print("Created Directory")
            else:
                os.mkdir(idenDirectory)
                print("Created Directory")
        except OSError:
            print("Creation of the directory failed")

        trainDirectory = os.path.join(self.currentDirectory, "idenskin", "train")
        try:
            if not os.path.exists(trainDirectory):
                os.mkdir(trainDirectory)
        except OSError:
            print("Creation of the directory failed")
        else:
            print("Successfully created the directory")

        testDirectory = os.path.join(self.currentDirectory, "idenskin", "test")
        try:
            if not os.path.exists(testDirectory):
                os.mkdir(testDirectory)
        except OSError:
            print("Creation of the directory failed")
        else:
            print("Successfully created the directory")

        malignantTrainDirectory = os.path.join(self.currentDirectory, "idenskin", "train", "malignant")
        malignantTestDirectory = os.path.join(self.currentDirectory, "idenskin", "test", "malignant")
        try:
            if not os.path.exists(malignantTrainDirectory) and not os.path.exists(malignantTestDirectory):
                os.mkdir(malignantTrainDirectory)
                os.mkdir(malignantTestDirectory)
        except OSError:
            print("Creation of the directory failed")
        else:
            print("Successfully created the directory")

        benignTrainDirectory = os.path.join(self.currentDirectory, "idenskin", "train", "benign")
        benignTestDirectory = os.path.join(self.currentDirectory, "idenskin", "test", "benign")
        try:
            if not os.path.exists(benignTrainDirectory) and not os.path.exists(benignTestDirectory):
                os.mkdir(benignTrainDirectory)
                os.mkdir(benignTestDirectory)
        except OSError:
            print("Creation of the directory failed")
        else:
            print("Successfully created the directory")

    def sortDirectories(self):
        trainDirectory = os.path.join(self.currentDirectory, "idenskin", "train")
        testDirectory = os.path.join(self.currentDirectory, "idenskin", "test")

        fileData = pandas.read_csv(
            os.path.join(self.currentDirectory, "skin-cancer-mnist-ham10000", "HAM10000_metadata.csv"))
        print(fileData.head())

        groupedData = fileData.groupby('lesion_id').count()
        groupedData = groupedData[groupedData['image_id'] == 1]
        groupedData.reset_index(inplace=True)
        print(groupedData.head())

        def findDuplicates(x):
            unique_list = list(groupedData['lesion_id'])
            if x in unique_list:
                return 'unique'
            else:
                return 'duplicates_found'

        fileData['duplicates'] = fileData['lesion_id']
        fileData['duplicates'] = fileData['duplicates'].apply(findDuplicates)
        print(fileData.head())

        print(fileData['duplicates'].value_counts())

        groupedData = fileData[fileData['duplicates'] == 'unique']
        print(groupedData.shape)

        temp = groupedData['dx']
        _, groupedDataVal = train_test_split(groupedData, test_size=0.30, random_state=100, stratify=temp)
        print(groupedDataVal.shape)
        print(groupedDataVal['dx'].value_counts())

        def findValRows(x):
            valList = list(groupedDataVal['image_id'])
            if str(x) in valList:
                return 'val'
            else:
                return 'train'

        fileData['train_or_val'] = fileData['image_id']
        fileData['train_or_val'] = fileData['train_or_val'].apply(findValRows)
        groupedDataTrain = fileData[fileData['train_or_val'] == 'train']
        print(len(groupedDataTrain))
        print(len(groupedDataVal))

        print(groupedDataTrain['dx'].value_counts())
        print(groupedDataVal['dx'].value_counts())

        fileData.set_index('image_id', inplace=True)

        files_part1 = os.listdir(
            os.path.join(self.currentDirectory, "skin-cancer-mnist-ham10000", "HAM10000_images_part_1"))
        files_part2 = os.listdir(
            os.path.join(self.currentDirectory, "skin-cancer-mnist-ham10000", "HAM10000_images_part_2"))

        trainingList = list(groupedDataTrain['image_id'])
        validate_list = list(groupedDataVal['image_id'])

        for img in trainingList:
            fileName = img + '.jpg'
            label = fileData.loc[img, 'dx']

            if fileName in files_part1:
                source = os.path.join(self.currentDirectory, "skin-cancer-mnist-ham10000", "HAM10000_images_part_1", fileName)
                if label == 'bcc' or label == 'mel' or label == 'akiec':
                    destination = os.path.join(trainDirectory, 'malignant', fileName)
                    copyfile(source, destination)
                if label == 'bkl' or label == 'df' or label == 'nv' or label == 'vasc':
                    destination = os.path.join(trainDirectory, 'benign', fileName)
                    copyfile(source, destination)

            if fileName in files_part2:
                source = os.path.join(self.currentDirectory, "skin-cancer-mnist-ham10000", "HAM10000_images_part_2",fileName)
                if label == 'bcc' or label == 'mel' or label == 'akiec':
                    destination = os.path.join(trainDirectory, 'malignant', fileName)
                    copyfile(source, destination)
                if label == 'bkl' or label == 'df' or label == 'nv' or label == 'vasc':
                    destination = os.path.join(trainDirectory, 'benign', fileName)
                    copyfile(source, destination)

        for img in validate_list:
            fileName = img + '.jpg'
            label = fileData.loc[img, 'dx']

            if fileName in files_part1:
                source = os.path.join(self.currentDirectory, "skin-cancer-mnist-ham10000", "HAM10000_images_part_1",
                                      fileName)
                if label == 'bcc' or label == 'mel' or label == 'akiec':
                    destination = os.path.join(testDirectory, 'malignant', fileName)
                    copyfile(source, destination)
                if label == 'bkl' or label == 'df' or label == 'nv' or label == 'vasc':
                    destination = os.path.join(testDirectory, 'benign', fileName)
                    copyfile(source, destination)

            if fileName in files_part2:
                source = os.path.join(self.currentDirectory, "skin-cancer-mnist-ham10000", "HAM10000_images_part_2",
                                      fileName)
                if label == 'bcc' or label == 'mel' or label == 'akiec':
                    destination = os.path.join(testDirectory, 'malignant', fileName)
                    copyfile(source, destination)
                if label == 'bkl' or label == 'df' or label == 'nv' or label == 'vasc':
                    destination = os.path.join(testDirectory, 'benign', fileName)
                    copyfile(source, destination)
